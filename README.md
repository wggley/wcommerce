# wCommerce

Reactjs, responsive, mobile-first e-commerce, using payments api from [pagar.me](https://pagar.me)

## Running Instructions

Follow below instructions to get a working project copy for local execution.

### Requirements

```
Git client
Nodejs 12.14.1 or higher
Npm 6.13.4 or higher
Pagarme Tests Account
```

### Install and Configuration

Run below command on your prompt to get the repo:

`
git clone https://wggley@bitbucket.org/wggley/wcommerce.git
`

Pagarme's Api configuration are stored on the file:

`
src\api.json
`

Open and edit this file with the following attributes marked with (?):

```javascript
{
    "api_key": "?", // api_key pagarme's test api key
    "customer": {
        "id": "?" // valid customer id
    },
    "installments": {
        "free_installments": "?", // total installments interest rate tax that won't be charged from credit card
        "max_installments": "?", // max available installments for credit card
        "interest_rate": ? // interest rate applied over installments
    },
    "split_rules": [{
            "recipient_id": "?", // seller's recipient_id
            "percentage": 85,
            "liable": true,
            "charge_processing_fee": true
        },
        {
            "recipient_id": "?", // marketplace1s recipient_id
            "percentage": 15,
            "liable": false,
            "charge_processing_fee": false
        }
    ]
}
```

It's also possible to edit products mocked data on file:

`src\products.json`


After all configurations are done, run following commands on the root project path `wcommerce`:

`
npm install
`

`
npm start
`



These commands shall install all project dependencies and open the app on developer mode over the url: 

[http://localhost:3000](http://localhost:3000)


## Running automated tests

Run below command to start tests:

`npm test`

On your OS prompt will be displayed all tests status.


## Developed with:

* [Pagar.me JS Api](https://pagarme.github.io/pagarme-js/)
* [Reactjs](https://reactjs.org/)
* [Sass](https://sass-lang.com/)
* [Npm](https://www.npmjs.com/)
* [Create React App](https://create-react-app.dev/)
* [JEST](https://jestjs.io/)
