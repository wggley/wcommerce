import React from 'react'
import MaskedInput from 'react-text-mask'
import './FormField.scss'

class FormField extends React.Component {
    renderHeader() {
        return (
            <div className="header">
                <div className="title">{this.props.title}</div>
            </div>
        )
    }

    renderError() {
        const errorMessage = this.props.errorMessage ? this.props.errorMessage : 'Campo ' + this.props.name + ' obrigatório.'
        if (this.props.hasError) {
            return (
                <span className="error">
                    {errorMessage}
                </span>
            )
        }

        return null
    }

    renderInput() {
        return (
            <div key={this.props.id} className={this.props.id}>
                <label>{this.props.name}</label>
                <input
                    type="text"
                    name={this.props.id}
                    onChange={this.props.onChange}
                    onBlur={this.props.onBlur}
                    onFocus={this.props.onFocus}
                    className={this.props.hasError ? 'has-error' : ''}
                    ref={this.props.reactRef}
                    value={this.props.value}
                    maxLength={this.props.maxLength}
                    disabled={this.props.disabled}
                />
                {this.renderError()}
            </div>
        )
    }

    renderSelect() {
        return (
            <div key={this.props.id} className={this.props.id}>
                <label>{this.props.name}</label>
                <select
                    name={this.props.id}
                    onChange={this.props.onChange}
                    onBlur={this.props.onBlur}
                    onFocus={this.props.onFocus}
                    className={this.props.hasError ? 'has-error' : ''}
                    ref={this.props.reactRef}
                    value={this.props.value}
                    disabled={this.props.disabled}
                >
                    {this.props.options}
                </select>
                {this.renderError()}
            </div>
        )
    }

    renderMaskedInput() {
        return (
            <div key={this.props.id} className={this.props.id}>
                <label>{this.props.name}</label>
                <MaskedInput
                    name={this.props.id}
                    mask={this.props.mask}
                    guide={this.props.guide}
                    onChange={this.props.onChange}
                    onBlur={this.props.onBlur}
                    onFocus={this.props.onFocus}
                    className={this.props.hasError ? 'has-error' : ''}
                    ref={this.props.reactRef}
                    value={this.props.value}
                    disabled={this.props.disabled}
                />
                {this.renderError()}
            </div>
        )
    }

    render() {
        let renderComponent
        switch (this.props.type) {
            case 'header':
                renderComponent = this.renderHeader()
                break
            case 'input':
                renderComponent = this.renderInput()
                break
            case 'select':
                renderComponent = this.renderSelect()
                break
            case 'masked-input':
                renderComponent = this.renderMaskedInput()
                break
            case 'error':
                renderComponent = this.renderError()
                break
            default:
                renderComponent = null
                break
        }
        return (renderComponent)
    }
}

export default FormField
