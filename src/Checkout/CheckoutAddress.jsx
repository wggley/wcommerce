import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { MdCheck } from 'react-icons/md'
import pagarme from 'pagarme/browser'
import './CheckoutAddress.scss'
import FormField from '../FormField/FormField.jsx'
import apiData from '../api.json'
import jsonData from './states.json'

const statesData = JSON.parse(JSON.stringify(jsonData))

class CheckoutAddress extends React.Component {
    constructor(props) {
        super(props)

        this.apiData = JSON.parse(JSON.stringify(apiData))

        this.state = (props.checkoutAddress ? props.checkoutAddress : {
            zipcode: '',
            to: '',
            street: '',
            street_number: '',
            complementary: '',
            neighborhood: '',
            city: '',
            state: '',
            country: 'br',
            processing: false
        })

        this.state.errors = {}
        this.state.formError = ''

        this.oldZipCode = this.state.zipcode

        for (let key in this.state) {
            if (this.state.hasOwnProperty(key)) {
                this[key + 'Ref'] = React.createRef()
            }
        }
    }

    getError(result) {
        let error = result.message
        if (result.response && result.response.errors) {
            if (result.response.errors[0].parameter_name) {
                error = result.response.errors[0].parameter_name
            }
            error += ' ' + result.response.errors[0].message
        }
        return error
    }

    findZipCode() {
        if (this.oldZipCode === this.state.zipcode || this.state.zipcode.search('_') > -1) {
            return
        }
        this.oldZipCode = this.state.zipcode
        this.setState({
            formError: 'Carregando dados do CEP...',
            processing: true
        })
        pagarme.client.connect({ api_key: this.apiData.api_key })
            .then(client => {
                return client.zipcodes.find({
                    zipcode: this.state.zipcode
                })
            })
            .then(response => {
                this.setState({
                    street: response.street,
                    neighborhood: response.neighborhood,
                    city: response.city,
                    state: response.state,
                    formError: '',
                    processing: false
                })
                this.toRef.current.focus()
            }, result => {
                console.log(result)
                const formError = (<span> Erro ao consultar dados de CEP: <br /> {this.getError(result)} </span>)
                this.setState({
                    formError: formError,
                    processing: false
                })
            })
    }

    getInstallments() {
        return pagarme.client.connect({ api_key: this.apiData.api_key })
            .then(client => client.transactions.calculateInstallmentsAmount({
                max_installments: this.apiData.installments.max_installments,
                free_installments: this.apiData.installments.free_installments,
                interest_rate: this.apiData.installments.interest_rate,
                amount: this.props.calculateAmount(this.props.shoppingCartItems)
            }))
    }

    handleFormSubmit(event) {
        event.preventDefault()

        this.setState({
            formError: '',
            processing: false
        })

        const ignoredKeys = ['formError', 'errors', 'complementary', 'processing']
        let errors = {}
        for (let key in this.state) {
            if (!this.state[key] && ignoredKeys.indexOf(key) < 0) {
                errors[key] = true
            }
        }

        if (this.state.zipcode.search('_') > -1) {
            errors.zipcode = true
        }

        this.setState({
            errors: errors
        })

        const errorKeys = Object.keys(errors)
        if (errorKeys.length > 0) {
            let element = this[errorKeys[0] + 'Ref'].current
            if (element.inputElement) {
                element = element.inputElement
            }
            element.focus()
            window.scrollTo(0, element.offsetTop - 200)
        }

        if (errorKeys.length > 0) {
            this.setState({
                formError: 'Formulário possui campos não preenchidos.'
            })
            return
        }

        this.setState({
            formError: 'Obtendo dados de Parcelas...',
            processing: true
        })

        this.getInstallments()
            .then(response => {
                this.setState({
                    zipcode: this.state.zipcode.replace('-', '')
                })

                let checkoutAddress = Object.assign({}, this.state)
                checkoutAddress.zipcode = checkoutAddress.zipcode.replace('-', '')
                delete checkoutAddress.errors
                delete checkoutAddress.formError
                delete checkoutAddress.processing
                if (checkoutAddress.complementary === '') {
                    delete checkoutAddress.complementary
                }

                this.props.handleEditCheckoutAddress(checkoutAddress, response.installments)
                this.props.history.push('/checkout/card')
            })
            .catch(result => {
                console.log(result)
                const formError = (<span> Erro ao consultar dados de parcelas: <br /> {this.getError(result)} </span>)
                this.setState({
                    formError: formError,
                    processing: false
                })
            })
    }

    handleOnChange(event) {
        let { name, value } = event.target

        this.setState({
            [name]: value
        })
    }

    renderButtons() {
        return (
            <div className="buttons">
                <Link to={'/shopping-cart'}>
                    <button>
                        Voltar
                    </button>
                </Link>
                <button className="button-continue" onClick={event => this.handleFormSubmit(event)}>
                    <MdCheck className="icon" />
                    Continuar
                </button>
            </div>
        )
    }

    renderStates() {
        let statesOptions = []
        statesOptions.push(
            <option
                key={-1}
                value=""
            />
        )
        for (let i = 0; i < statesData.length; i++) {
            statesOptions.push(
                <option
                    key={i}
                    value={statesData[i]}
                >
                    {statesData[i]}
                </option>
            )
        }
        return statesOptions
    }

    render() {
        return (
            <div className="App-checkout-address has-header">
                <form>
                    <FormField
                        type="header"
                        title="Endereço para Entrega"
                    />
                    <FormField
                        type="masked-input"
                        onChange={event => this.handleOnChange(event)}
                        onBlur={() => this.findZipCode()}
                        hasError={this.state.errors.zipcode}
                        id="zipcode"
                        name="CEP"
                        mask={[/[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, /[0-9]/]}
                        reactRef={this.zipcodeRef}
                        value={this.state.zipcode}
                        disabled={this.state.processing}
                    />
                    <FormField
                        type="input"
                        onChange={event => this.handleOnChange(event)}
                        hasError={this.state.errors.to}
                        id="to"
                        name="Destinatário"
                        reactRef={this.toRef}
                        value={this.state.to}
                        disabled={this.state.processing}
                    />
                    <FormField
                        type="input"
                        onChange={event => this.handleOnChange(event)}
                        hasError={this.state.errors.street}
                        id="street"
                        name="Rua"
                        reactRef={this.streetRef}
                        value={this.state.street}
                        disabled={this.state.processing}
                    />
                    <FormField
                        type="input"
                        onChange={event => this.handleOnChange(event)}
                        hasError={this.state.errors.street_number}
                        id="street_number"
                        name="Nº"
                        reactRef={this.street_numberRef}
                        value={this.state.street_number}
                        disabled={this.state.processing}
                    />
                    <FormField
                        type="input"
                        onChange={event => this.handleOnChange(event)}
                        hasError={this.state.errors.complementary}
                        id="complementary"
                        name="Complemento"
                        reactRef={this.complementaryRef}
                        value={this.state.complementary}
                        disabled={this.state.processing}
                    />
                    <FormField
                        type="input"
                        onChange={event => this.handleOnChange(event)}
                        hasError={this.state.errors.neighborhood}
                        id="neighborhood"
                        name="Bairro"
                        reactRef={this.neighborhoodRef}
                        value={this.state.neighborhood}
                        disabled={this.state.processing}
                    />
                    <FormField
                        type="input"
                        onChange={event => this.handleOnChange(event)}
                        hasError={this.state.errors.city}
                        id="city"
                        name="Cidade"
                        reactRef={this.cityRef}
                        value={this.state.city}
                        disabled={this.state.processing}
                    />
                    <FormField
                        type="select"
                        onChange={event => this.handleOnChange(event)}
                        hasError={this.state.errors.state}
                        id="state"
                        name="Estado"
                        options={this.renderStates()}
                        reactRef={this.stateRef}
                        value={this.state.state}
                        disabled={this.state.processing}
                    />
                    <div className="submit-box">
                        {
                            (Object.keys(this.state.errors).length) || this.state.formError !== '' ?
                                <span className={'form-error ' + (this.state.processing ? 'processing' : '')}>
                                    {this.state.formError}
                                </span>
                                : null
                        }
                        {this.renderButtons()}
                    </div>
                </form>
            </div>
        )
    }
}

export default withRouter(CheckoutAddress)
