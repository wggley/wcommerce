import React from 'react'
import './CheckoutEnd.scss'
import { Link } from 'react-router-dom'
import { MdShoppingCart } from 'react-icons/md'
import FormField from '../FormField/FormField.jsx'
import apiData from '../api.json'

class CheckoutEnd extends React.Component {
    constructor(props) {
        super(props)

        this.apiData = JSON.parse(JSON.stringify(apiData))
    }

    componentDidMount() {
        this.props.handleResetData()
    }

    renderHeader() {
        return (
            <div key={0} className="list-header">
                <div className="cart">Itens</div>
                <div className="cart-quantity">Quantidade</div>
                <div className="cart-price">Preço</div>
            </div>
        )
    }

    renderProduct(item, index) {
        return (
            <div key={item.id} className="item">
                <div className="item-title">
                    {item.title}
                </div>
                <div className="item-quantity">
                    <span className="item-quantity-label">Quantidade:</span>
                    <span className="item-quantity-value">{item.quantity}</span>
                </div>
                <div className="item-value">
                    <p className="item-value-total">{this.props.parsePrice(item.quantity * item.unit_price)}</p>
                    <p className="item-value-unit-price">{this.props.parsePrice(item.unit_price)} unid.</p>
                </div>
            </div>
        )
    }

    renderTotal() {
        return (
            <div key={this.props.transaction.items.length + 1} className="total item">
                <span className="total-title">
                    Total {this.props.transaction.installments > this.apiData.installments.free_installments ? '(com juros)' : ''}
                </span>
                <span className="total-quantity">
                    {this.props.getTotalItems(this.props.transaction.items)}
                    {this.props.getTotalItems(this.props.transaction.items) === 1 ? ' item' : ' itens'}
                </span>
                <span className="total-value">
                    {this.props.parsePrice(this.props.transaction.amount)}
                </span>
            </div>
        )
    }

    renderTotalRecipients() {
        return (
            <div key={this.props.transaction.items.length + 2} className="recipient item">
                <span className="recipient-title">
                    Total Recebido
                </span>
                <div className="recipient-actor">
                    Plataforma
                </div>
                <div className="recipient-value">
                    {this.props.parsePrice(this.props.amountPlatform)}
                </div>
                <span className="recipient-actor">
                    Vendedor
                </span>
                <span className="recipient-value">
                    {this.props.parsePrice(this.props.amountSeller)}
                </span>
            </div>
        )
    }

    renderCheckoutButton() {
        return (
            <Link to={'/'}>
                <button className="button-home">
                    <MdShoppingCart className="button-home-icon" />
                    Fazer mais compras
                </button>
            </Link>
        )
    }

    render() {
        let rows = []
        if (this.props.transaction.items.length > 0) {
            rows.push(this.renderHeader())
            for (let i = 0; i < this.props.transaction.items.length; i++) {
                rows.push(this.renderProduct(this.props.transaction.items[i], i))
            }
        }

        return (
            <div className="App-checkout-end has-header">
                <FormField
                    type="header"
                    title="Compra Finalizada com Sucesso!"
                />
                <div className="list">
                    <div className="order-title">
                        Pedido #{this.props.transaction.id}
                    </div>
                    {rows}
                    {this.renderTotal()}
                    {this.renderTotalRecipients()}
                </div>
                {this.renderCheckoutButton()}
            </div>
        )
    }
}

export default CheckoutEnd
