import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import Cards from 'react-credit-cards'
import pagarme from 'pagarme/browser'
import { MdCheck } from 'react-icons/md'
import './CheckoutCard.scss'
import FormField from '../FormField/FormField.jsx'
import apiData from '../api.json'

class CheckoutCard extends React.Component {
    constructor(props) {
        super(props)

        this.apiData = JSON.parse(JSON.stringify(apiData))

        this.state = {
            number: '',
            name: '',
            expiry: '',
            cvc: '',
            installments: '',
            focus: '',
            amount: '',
            processing: false
        }

        this.state.errors = {}
        this.state.formError = ''

        for (let key in this.state) {
            if (this.state.hasOwnProperty(key)) {
                this[key + 'Ref'] = React.createRef()
            }
        }
    }

    getCardMask() {
        return [
            /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, ' ',
            /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, ' ',
            /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, ' ',
            /[0-9]/, /[0-9]/, /[0-9]/, /[0-9]/, ' ',
            /[0-9]/, /[0-9]/, /[0-9]/
        ]
    }

    getError(result) {
        let error = result.message
        if (result.response && result.response.errors) {
            if (result.response.errors[0].parameter_name) {
                error = result.response.errors[0].parameter_name
            }
            error += ' ' + result.response.errors[0].message
        }
        return error
    }

    getPayables(transaction) {
        return pagarme.client.connect({ api_key: this.apiData.api_key })
            .then(client => client.payables.find({
                transactionId: transaction.id
            }))
            .then(response => {
                let amountSeller = 0
                let amountPlatform = 0

                for (let i = 0; i < response.length; i++) {
                    if (response[i].recipient_id === transaction.split_rules[0].recipient_id) {
                        amountPlatform += response[i].amount
                    }
                    if (response[i].recipient_id === transaction.split_rules[1].recipient_id) {
                        amountSeller += response[i].amount
                    }
                }

                transaction.amountSeller = amountSeller
                transaction.amountPlatform = amountPlatform

                return transaction
            }, result => {
                console.log(result)
                const formError = (<span> Erro ao consultar dados de Recebedores: <br /> {this.getError(result)} </span>)
                this.setState({
                    formError: formError,
                    processing: false
                })
            })
    }

    setExpiryMask(rawValue) {
        let date = new Date()
        let dateYear = '' + date.getFullYear()
        let yearMaskFirstDigit = new RegExp('[' + dateYear[2] + '-9]', '')
        let yearMaskLastDigit = new RegExp('[0-9]', '')
        if (rawValue[3] === dateYear[2]) {
            yearMaskLastDigit = new RegExp('[' + dateYear[3] + '-9]', '')
        }
        if (rawValue[0] === '0') {
            return [/[0-1]/, /[1-9]/, '/', yearMaskFirstDigit, yearMaskLastDigit]
        }
        if (rawValue === '') {
            return [/[0-1]/, /[0-9]/, '/', yearMaskFirstDigit, yearMaskLastDigit]
        }
        return [/[0-1]/, /[0-2]/, '/', yearMaskFirstDigit, yearMaskFirstDigit]
    }

    handleFormSubmit(event) {
        event.preventDefault()

        this.setState({
            formError: '',
            processing: false
        })

        const ignoredKeys = ['formError', 'errors', 'focus', 'amount', 'processing']
        let errors = {}
        for (let key in this.state) {
            if (!this.state[key] && ignoredKeys.indexOf(key) < 0) {
                errors[key] = true
            }
        }

        if (this.state.number.length < 16) {
            errors.number = true
        }

        if (this.state.expiry.search('_') > -1) {
            errors.expiry = true
        }

        if (this.state.cvc.search('_') > -1) {
            errors.cvc = true
        }

        this.setState({
            errors: errors
        })

        const errorKeys = Object.keys(errors)
        if (errorKeys.length > 0) {
            let element = this[errorKeys[0] + 'Ref'].current
            if (element.inputElement) {
                element = element.inputElement
            }
            element.focus()
            window.scrollTo(0, element.offsetTop - 200)
        }

        if (errorKeys.length > 0) {
            this.setState({
                formError: 'Formulário possui campos não preenchidos.'
            })
            return
        }

        this.setState({
            formError: 'Processando sua transação junto à operadora de cartão de crédito...',
            processing: true
        })

        pagarme.client.connect({ api_key: this.apiData.api_key })
            .then(client => this.createTransaction(client))
            .then(transaction => this.getPayables(transaction))
            .then(transaction => {
                const validStatus = ['processing', 'authorized', 'paid', 'analyzing', 'pending_review']
                console.log(transaction)
                if (validStatus.indexOf(transaction.status) === -1) {
                    const formError = (<span> Não foi possível completar a sua transação: <br /> Cartão recusado, entre em contato com sua operadora de cartão ou informe outro cartão válido. </span>)
                    this.setState({
                        formError: formError,
                        processing: false
                    })
                    return
                }
                this.props.handleEditTransaction(transaction)
                this.props.history.push('/checkout/end')
            }, result => {
                console.log(result)
                const formError = (<span> Erro ao criar a transação: <br /> {this.getError(result)} </span>)
                this.setState({
                    formError: formError,
                    processing: false
                })
            })
    }

    createTransaction(client) {
        const card = Object.assign({}, this.state)
        let address = Object.assign({}, this.props.checkoutAddress)
        const customerName = this.props.checkoutAddress.to
        delete address.to

        const billing = {
            name: this.state.name,
            address: address
        }

        let date = new Date()
        let year = date.getFullYear()
        let month = (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1)
        let day = (date.getDate() < 10 ? '0' : '') + date.getDate()
        let deliveryDate = `${year}-${month}-${day}`

        const shipping = {
            name: customerName,
            fee: 0,
            delivery_date: deliveryDate,
            expedited: false,
            address: address
        }

        let items = []
        for (let i = 0; i < this.props.shoppingCartItems.length; i++) {
            items.push({
                id: this.props.shoppingCartItems[i].id,
                title: this.props.shoppingCartItems[i].title,
                unit_price: this.props.shoppingCartItems[i].unit_price,
                tangible: this.props.shoppingCartItems[i].tangible,
                quantity: this.props.shoppingCartItems[i].quantity
            })
        }

        return client.transactions.create({
            amount: this.state.amount,
            payment_method: 'credit_card',
            card_number: card.number,
            card_holder_name: card.name,
            card_expiration_date: card.expiry.replace('/', ''),
            card_cvv: card.cvc,
            installments: card.installments,
            split_rules: this.apiData.split_rules,
            customer: this.apiData.customer,
            billing: billing,
            shipping: shipping,
            items: items
        })
    }

    handleOnFocus(event) {
        this.setState({
            focus: event.target.name
        })
    }

    handleOnChange(event) {
        let { name, value } = event.target

        if (name === 'number') {
            value = value.replace(/[ _]/g, '')
        }

        if (name === 'installments') {
            this.setState({
                amount: this.props.installmentsCalc[value].amount
            })
        }

        this.setState({
            [name]: value
        })
    }

    parseInterestRate(installment) {
        if (installment === 1) {
            return 'À vista '
        }
        return installment.toString() + 'x ' + (installment > Number(this.apiData.installments.free_installments) ? ' com juros ' : ' sem juros ')
    }

    getInstallmentAmount(index) {
        return this.props.installmentsCalc[index].installment_amount
    }

    renderInstallments() {
        let options = []
        options.push(
            <option
                key={0}
                value={''}
            >
                Selecione
            </option>
        )
        for (let i = 1; i < 13; i++) {
            options.push(
                <option
                    key={i}
                    value={i}
                >
                    {this.parseInterestRate(i)} {this.props.parsePrice(this.getInstallmentAmount(i))}
                </option>
            )
        }
        return options
    }

    renderButtons() {
        return (
            <div className="buttons">
                <Link to={'/checkout/address'}>
                    <button>
                        Voltar
                    </button>
                </Link>
                <button className="button-end" onClick={event => this.handleFormSubmit(event)}>
                    <MdCheck className="icon" />
                    Finalizar
                </button>
            </div>
        )
    }

    render() {
        return (
            <div className="App-checkout-card has-header">
                <form>
                    <FormField
                        type="header"
                        title="Dados do Cartão de Crédito"
                    />
                    <Cards
                        focused={this.state.focus}
                        cvc={this.state.cvc}
                        expiry={this.state.expiry}
                        name={this.state.name}
                        number={this.state.number}
                        placeholders ={{ name: 'SEU NOME' }}
                    />
                    <div className="card-data">
                        <FormField
                            type="masked-input"
                            guide={false}
                            onChange={event => this.handleOnChange(event)}
                            onFocus={event => this.handleOnFocus(event)}
                            hasError={this.state.errors.number}
                            id="number"
                            name="Número"
                            mask={this.getCardMask()}
                            reactRef={this.numberRef}
                            disabled={this.state.processing}
                        />
                        <FormField
                            type="input"
                            onChange={event => this.handleOnChange(event)}
                            onFocus={event => this.handleOnFocus(event)}
                            hasError={this.state.errors.name}
                            id="name"
                            name="Nome"
                            reactRef={this.nameRef}
                            maxLength={18}
                            disabled={this.state.processing}
                        />
                        <FormField
                            type="masked-input"
                            onChange={event => this.handleOnChange(event)}
                            onFocus={event => this.handleOnFocus(event)}
                            hasError={this.state.errors.expiry}
                            id="expiry"
                            name="Validade"
                            mask={this.setExpiryMask}
                            reactRef={this.expiryRef}
                            disabled={this.state.processing}
                        />
                        <FormField
                            type="masked-input"
                            onChange={event => this.handleOnChange(event)}
                            onFocus={event => this.handleOnFocus(event)}
                            hasError={this.state.errors.cvc}
                            id="cvc"
                            name="CVC / CVV"
                            mask={[/[0-9]/, /[0-9]/, /[0-9]/]}
                            reactRef={this.cvcRef}
                            disabled={this.state.processing}
                        />
                        <FormField
                            type="select"
                            onChange={event => this.handleOnChange(event)}
                            hasError={this.state.errors.installments}
                            id="installments"
                            name="Parcelas"
                            options={this.renderInstallments()}
                            reactRef={this.installmentsRef}
                            disabled={this.state.processing}
                        />
                    </div>
                    <div className="submit-box">
                        {
                            (Object.keys(this.state.errors).length) || this.state.formError !== '' ?
                                <span className={'form-error ' + (this.state.processing ? 'processing' : '')}>
                                    {this.state.formError}
                                </span>
                                : null
                        }
                        {this.renderButtons()}
                    </div>
                </form>
            </div>
        )
    }
}

export default withRouter(CheckoutCard)
