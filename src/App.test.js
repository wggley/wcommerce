import React from 'react'
import { render, fireEvent, wait } from '@testing-library/react'
import { mount } from 'enzyme'
import App from './App.jsx'

let shoppingCartItems = 0

test('renders header', () => {
    const { getByText } = render(<App />)
    const element = getByText(/wCommerce/i)
    expect(element).toBeInTheDocument()
})

test('renders header shopping cart link', () => {
    const { container } = render(<App />)
    const element = container.querySelector('.shopping-cart-icon-count-items')
    expect(element).toBeInTheDocument()
})

test('renders product list', () => {
    const { container } = render(<App />)
    const element = container.querySelector('.App-product-list')
    expect(element).toBeInTheDocument()
})

test('renders product', () => {
    const { container } = render(<App />)
    const element = container.querySelector('.App-product')
    expect(element).toBeInTheDocument()
})

test('renders add to cart button', () => {
    const { container } = render(<App />)
    const element = container.querySelector('.button-add-to-cart')
    expect(element).toBeInTheDocument()
})

test('renders product details button', () => {
    const { container } = render(<App />)
    const element = container.querySelector('.button-product-details')
    expect(element).toBeInTheDocument()
})

test('show product-details', () => {
    const wrapper = mount(<App />)
    const button = wrapper.find('.App-product a').first()
    button.simulate('click', { button: 0 }) // needed for react-router
    wrapper.setProps({})
    expect(wrapper.exists('.product-vendor-id-label')).toBeTruthy()
})

test('add item to shopping-cart', () => {
    const wrapper = mount(<App />)
    const button = wrapper.find('.App-product .button-add-to-cart').first()
    shoppingCartItems++
    button.simulate('click') // needed for react-router
    wrapper.setProps({})
    const counter = wrapper.find('.shopping-cart-icon-count-items').text()
    expect(counter).toEqual('1')
})

test('go back to home', () => {
    const wrapper = mount(<App />)
    const button = wrapper.find('.App-header a').first()
    button.simulate('click', { button: 0 }) // needed for react-router
    wrapper.setProps({})
    expect(wrapper.exists('.App-product-list')).toBeTruthy()
})

test('add all items to shopping-cart', () => {
    const wrapper = mount(<App />)
    const buttons = wrapper.find('.App-product .button-add-to-cart')

    buttons.forEach(function(element, index) {
        element.simulate('click')
        shoppingCartItems++
    })

    wrapper.setProps({})
    const counter = wrapper.find('.shopping-cart-icon-count-items').text()
    expect(counter).toEqual('' + shoppingCartItems)
})

test('go to shopping-cart', () => {
    const wrapper = mount(<App />)
    const button = wrapper.find('.App-header a').first()
    button.simulate('click', { button: 0 }) // needed for react-router
    wrapper.setProps({})
    expect(wrapper.exists('.App-shopping-cart')).toBeTruthy()
})

test('remove all items from shopping cart and go back to home', () => {
    const wrapper = mount(<App />)
    const buttons = wrapper.find('.App-shopping-cart .button-remove-from-cart')

    buttons.forEach(function(element, index) {
        element.simulate('click')
    })

    shoppingCartItems = 0
    wrapper.setProps({})

    const button = wrapper.find('.App-header a').first()
    button.simulate('click', { button: 0 }) // needed for react-router

    wrapper.setProps({})

    expect(wrapper.exists('.App-product-list')).toBeTruthy()

    const counter = wrapper.find('.shopping-cart-icon-count-items').text()
    expect(counter).toEqual('' + shoppingCartItems)
})

test('add item to shopping-cart and edit quantity to 2 and go to checkout', () => {
    const { container, getByText } = render(<App />)

    fireEvent.click(container.querySelectorAll('.App-product .button-add-to-cart')[3])

    fireEvent.click(container.querySelector('.App-header a'))

    const element = container.querySelector('.item-quantity-input')
    fireEvent.change(element, { target: { value: 2 } })
    fireEvent.blur(element)

    shoppingCartItems = 2
    expect(container.querySelector('.shopping-cart-icon-count-items').innerHTML).toEqual('' + shoppingCartItems)

    fireEvent.click(container.querySelector('.App-shopping-cart a'))

    expect(getByText('Endereço para Entrega')).toBeTruthy()
})

test('do checkout and create transaction', async () => {
    const { container, getByText } = render(<App />)

    const element = container.querySelector('.zipcode input')
    fireEvent.change(element, { target: { value: '03509-000' } })
    fireEvent.blur(element)

    await wait(() => expect(container.querySelector('.street input').value).toEqual('Avenida Marcondes de Brito'))
    expect(container.querySelector('.neighborhood input').value).toEqual('Chácara Seis de Outubro')
    expect(container.querySelector('.city input').value).toEqual('São Paulo')
    expect(container.querySelector('.state select').value).toEqual('SP')

    fireEvent.change(container.querySelector('.to input'), { target: { value: 'Xpto Linguiça LTDA.' } })
    fireEvent.change(container.querySelector('.street_number input'), { target: { value: '164' } })
    fireEvent.change(container.querySelector('.complementary input'), { target: { value: 'casa' } })

    fireEvent.click(container.querySelector('.button-continue'))

    await wait(() => expect(container.querySelector('.App-checkout-card')).toBeInTheDocument())

    fireEvent.change(container.querySelector('.number input'), { target: { value: '4111 1111 1111 1111' } })
    fireEvent.change(container.querySelector('.name input'), { target: { value: 'XPTO LINGUICA' } })
    fireEvent.change(container.querySelector('.expiry input'), { target: { value: '12/99' } })
    fireEvent.change(container.querySelector('.cvc input'), { target: { value: '100' } })

    fireEvent.change(container.querySelector('.installments select'), { target: { value: 6 } })

    fireEvent.click(container.querySelector('.button-end'))

    await wait(() => expect(getByText('Compra Finalizada com Sucesso!')).toBeInTheDocument())
})
