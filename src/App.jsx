import React from 'react'
import './App.scss'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams,
    useHistory
} from 'react-router-dom'
import ScrollToTop from './ScrollToTop.jsx'
import Header from './Header/Header.jsx'
import ProductList from './ProductList/ProductList.jsx'
import ProductDetails from './ProductDetails/ProductDetails.jsx'
import ShoppingCart from './ShoppingCart/ShoppingCart.jsx'
import CheckoutAddress from './Checkout/CheckoutAddress.jsx'
import CheckoutCard from './Checkout/CheckoutCard.jsx'
import CheckoutEnd from './Checkout/CheckoutEnd.jsx'
import productsData from './products.json'

class App extends React.Component {
    constructor(props) {
        super(props)

        window.lastLocation = ''
        window.lastLocationY = 0

        const products = JSON.parse(JSON.stringify(productsData))

        this.state = {
            products: products,
            shoppingCartItems: localStorage.getItem('shoppingCartItems') ? JSON.parse(localStorage.getItem('shoppingCartItems')) : [],
            checkoutAddress: localStorage.getItem('checkoutAddress') ? JSON.parse(localStorage.getItem('checkoutAddress')) : null,
            installmentsCalc: null,
            transaction: null,
            amountPlatform: null,
            amountSeller: null
        }

        this.handleAddCartItem = this.handleAddCartItem.bind(this)
        this.handleEditCartItem = this.handleEditCartItem.bind(this)
        this.handleRemoveCartItem = this.handleRemoveCartItem.bind(this)
        this.handleEditCheckoutAddress = this.handleEditCheckoutAddress.bind(this)
        this.handleEditTransaction = this.handleEditTransaction.bind(this)
        this.handleResetData = this.handleResetData.bind(this)
    }

    parsePrice(value) {
        value = value / 100
        let formatter = new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        })
        return formatter.format(value)
    }

    calculateAmount(items) {
        let amount = 0
        for (let i = 0; i < items.length; i++) {
            amount += items[i].unit_price * items[i].quantity
        }
        return amount
    }

    handleAddCartItem(item) {
        let shoppingCartItems = this.state.shoppingCartItems.slice()
        let found = -1
        for (let i = 0; i < shoppingCartItems.length; i++) {
            if (shoppingCartItems[i].id === item.id) {
                found = i
            }
        }
        if (found > -1) {
            shoppingCartItems[found].quantity++
        } else {
            shoppingCartItems = shoppingCartItems.concat(item)
            shoppingCartItems[shoppingCartItems.length - 1].quantity = 1
        }
        localStorage.setItem('shoppingCartItems', JSON.stringify(shoppingCartItems))
        this.setState({
            shoppingCartItems: shoppingCartItems
        })
    }

    handleEditCartItem(item, index) {
        let shoppingCartItems = this.state.shoppingCartItems.slice()
        shoppingCartItems[index] = item
        localStorage.setItem('shoppingCartItems', JSON.stringify(shoppingCartItems))
        this.setState({
            shoppingCartItems: shoppingCartItems
        })
    }

    handleRemoveCartItem(index) {
        let shoppingCartItems = this.state.shoppingCartItems.slice()
        shoppingCartItems.splice(index, 1)
        localStorage.setItem('shoppingCartItems', JSON.stringify(shoppingCartItems))
        this.setState({
            shoppingCartItems: shoppingCartItems
        })
    }

    handleEditCheckoutAddress(data, installments) {
        const checkoutAddress = Object.assign({}, data)
        const installmentsCalc = Object.assign({}, installments)
        localStorage.setItem('checkoutAddress', JSON.stringify(checkoutAddress))
        this.setState({
            checkoutAddress: checkoutAddress,
            installmentsCalc: installmentsCalc
        })
    }

    handleEditTransaction(data) {
        const transaction = Object.assign({}, data)
        this.setState({
            transaction: transaction,
            amountPlatform: transaction.amountPlatform,
            amountSeller: transaction.amountSeller
        })
    }

    handleResetData() {
        localStorage.removeItem('shoppingCartItems')
        localStorage.removeItem('checkoutAddress')
        this.setState({
            shoppingCartItems: [],
            checkoutAddress: null,
            installmentsCalc: null
        })
    }

    render() {
        return (
            <Router>
                <ScrollToTop />
                <Switch>
                    <Route
                        path="/product-details/:id"
                        children={
                            <ProductDetailsRoute
                                products={this.state.products}
                                shoppingCartItems={this.state.shoppingCartItems}
                                handleAddCartItem={this.handleAddCartItem}
                                parsePrice={this.parsePrice}
                            />
                        }
                    />
                    <Route
                        path="/shopping-cart"
                        children={
                            <ShoppingCartRoute
                                shoppingCartItems={this.state.shoppingCartItems}
                                handleEditCartItem={this.handleEditCartItem}
                                handleRemoveCartItem={this.handleRemoveCartItem}
                                calculateAmount={this.calculateAmount}
                                parsePrice={this.parsePrice}
                            />
                        }
                    />
                    <Route
                        path="/checkout/address"
                        children={
                            <CheckoutAddressRoute
                                shoppingCartItems={this.state.shoppingCartItems}
                                calculateAmount={this.calculateAmount}
                                checkoutAddress={this.state.checkoutAddress}
                                handleEditCheckoutAddress={this.handleEditCheckoutAddress}
                            />
                        }
                    />
                    <Route
                        path="/checkout/card"
                        children={
                            <CheckoutCardRoute
                                shoppingCartItems={this.state.shoppingCartItems}
                                checkoutAddress={this.state.checkoutAddress}
                                installmentsCalc={this.state.installmentsCalc}
                                calculateAmount={this.calculateAmount}
                                parsePrice={this.parsePrice}
                                handleEditTransaction={this.handleEditTransaction}
                            />
                        }
                    />
                    <Route
                        path="/checkout/end"
                        children={
                            <CheckoutEndRoute
                                shoppingCartItems={this.state.shoppingCartItems}
                                calculateAmount={this.calculateAmount}
                                parsePrice={this.parsePrice}
                                transaction={this.state.transaction}
                                amountPlatform={this.state.amountPlatform}
                                amountSeller={this.state.amountSeller}
                                handleResetData={this.handleResetData}
                            />
                        }
                    />
                    <Route
                        exact path="/"
                        children={
                            <HomeRoute
                                products={this.state.products}
                                shoppingCartItems={this.state.shoppingCartItems}
                                handleAddCartItem={this.handleAddCartItem}
                                parsePrice={this.parsePrice}
                            />
                        }
                    />
                </Switch>
            </Router>
        )
    }
}

function HomeRoute(props) {
    return (
        <div className="App">
            <Header
                count={getTotalItems(props.shoppingCartItems)}
            />
            <ProductList
                productsData={props.products}
                handleAddCartItem={props.handleAddCartItem}
                parsePrice={props.parsePrice}
            />
        </div>
    )
}

function ProductDetailsRoute(props) {
    const history = useHistory()
    const { id } = useParams()
    const product = getProduct(id, props.products)
    if (!product) {
        history.push('/')
        return null
    }
    return (
        <div className="App">
            <Header
                showHome={true}
                count={getTotalItems(props.shoppingCartItems)}
            />
            <ProductDetails
                product={product}
                handleAddCartItem={props.handleAddCartItem}
                parsePrice={props.parsePrice}
            />
        </div>
    )
}

function ShoppingCartRoute(props) {
    return (
        <div className="App shopping-cart">
            <Header
                showHome={true}
                count={getTotalItems(props.shoppingCartItems)}
            />
            <ShoppingCart
                shoppingCartItems={props.shoppingCartItems}
                getTotalItems={getTotalItems}
                handleEditCartItem={props.handleEditCartItem}
                handleRemoveCartItem={props.handleRemoveCartItem}
                calculateAmount={props.calculateAmount}
                parsePrice={props.parsePrice}
            />
        </div>
    )
}

function CheckoutAddressRoute(props) {
    const history = useHistory()
    if (props.shoppingCartItems.length === 0) {
        history.push('/')
        return null
    }
    return (
        <div className="App checkout-address">
            <Header
                showHome={true}
                count={getTotalItems(props.shoppingCartItems)}
            />
            <CheckoutAddress
                shoppingCartItems={props.shoppingCartItems}
                checkoutAddress={props.checkoutAddress}
                calculateAmount={props.calculateAmount}
                handleEditCheckoutAddress={props.handleEditCheckoutAddress}
            />
        </div>
    )
}

function CheckoutCardRoute(props) {
    const history = useHistory()
    if (props.shoppingCartItems.length === 0 || props.checkoutAddress === null || props.installmentsCalc === null) {
        history.push('/')
        return null
    }
    return (
        <div className="App checkout-card">
            <Header
                showHome={true}
                count={getTotalItems(props.shoppingCartItems)}
            />
            <CheckoutCard
                shoppingCartItems={props.shoppingCartItems}
                checkoutAddress={props.checkoutAddress}
                calculateAmount={props.calculateAmount}
                parsePrice={props.parsePrice}
                handleEditTransaction={props.handleEditTransaction}
                installmentsCalc={props.installmentsCalc}
            />
        </div>
    )
}

function CheckoutEndRoute(props) {
    const history = useHistory()
    if (props.transaction === null) {
        history.push('/')
        return null
    }
    return (
        <div className="App checkout-end">
            <Header
                showHome={true}
                count={getTotalItems(props.shoppingCartItems)}
            />
            <CheckoutEnd
                getTotalItems={getTotalItems}
                calculateAmount={props.calculateAmount}
                parsePrice={props.parsePrice}
                transaction={props.transaction}
                amountPlatform={props.amountPlatform}
                amountSeller={props.amountSeller}
                handleResetData={props.handleResetData}
            />
        </div>
    )
}

function getTotalItems(items) {
    let total = 0
    for (let i = 0; i < items.length; i++) {
        total += items[i].quantity
    }
    return total
}


function getProduct(id, products) {
    for (let i = 0; i < products.length; i++) {
        if (products[i].id === id) {
            return products[i]
        }
    }
    return null
}

export default App
