import React from 'react'
import './Header.scss'
import { Link } from 'react-router-dom'
import { MdHome } from 'react-icons/md'
import { MdShoppingCart } from 'react-icons/md'

class Header extends React.Component {
    getCount() {
        if (this.props.count === 0) {
            return <span className="shopping-cart-icon-count-items zero">0</span>
        }
        let count = this.props.count < 99 ? this.props.count : <span>&infin;</span>
        return <span className="shopping-cart-icon-count-items">{count}</span>
    }

    render() {
        let homeIcon = ''
        if (this.props.showHome) {
            homeIcon = (
                <Link to={'/'}>
                    <MdHome className="home-icon"/>
                </Link>
            )
        }
        return (
            <header className="App-header">
                <h1>
                    {homeIcon}
                    wCommerce
                    <Link to={'/shopping-cart'}>
                        {this.getCount()}
                        <MdShoppingCart className="shopping-cart-icon"/>
                    </Link>
                </h1>
            </header>
        )
    }
}

export default Header
