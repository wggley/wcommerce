import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'

export default function ScrollToTop() {
    const { pathname } = useLocation()
    let x = 0
    let y = 0
    if (window.lastLocation === '/') {
        window.lastLocationY = window.pageYOffset
    }
    if (pathname === '/') {
        y = window.lastLocationY
    }
    window.lastLocation = pathname

    useEffect(() => {
        window.scrollTo(x, y)
    }, [x, y, pathname])


    return null
}
