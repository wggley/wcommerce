import React from 'react'
import { Link } from 'react-router-dom'
import MaskedInput from 'react-text-mask'
import './ShoppingCart.scss'
import { MdCheck, MdDelete } from 'react-icons/md'

class ShoppingCart extends React.Component {
    handleBlur(event, item, index) {
        let value = Number(event.target.value)
        if (value === 0 || isNaN(value)) {
            value = 1
            event.target.value = value
        }
        item.quantity = value
        this.props.handleEditCartItem(item, index)
    }

    handleClick(index) {
        this.props.handleRemoveCartItem(index)
    }

    renderHeader() {
        return (
            <div key={0} className="list-header">
                <div className="cart">Carrinho</div>
                <div className="cart-quantity">Quantidade</div>
                <div className="cart-price">Preço</div>
            </div>
        )
    }

    renderProduct(item, index) {
        return (
            <div key={item.id} className="item">
                <div className="item-image">
                    <img alt="Imagem do Produto" src={item.image_path} />
                </div>
                <div className="item-title">
                    {item.title}
                </div>
                <div className="item-quantity">
                    <span className="item-quantity-label">Quantidade:</span>
                    <MaskedInput
                        mask={[/[0-9]/, /[0-9]/, /[0-9]/]}
                        guide={false}
                        onBlur={event => this.handleBlur(event, item, index)}
                        className="item-quantity-input"
                        value={item.quantity}
                    />
                    <button className="button-remove-from-cart" onClick={() => this.handleClick(index)}>
                        <MdDelete className="button-remove-from-cart-icon" />
                    </button>
                </div>
                <div className="item-value">
                    <p className="item-value-total">{this.props.parsePrice(item.quantity * item.unit_price)}</p>
                    <p className="item-value-unit-price">{this.props.parsePrice(item.unit_price)} unid.</p>
                </div>
            </div>
        )
    }

    renderTotal(items) {
        let totalValue = this.props.calculateAmount(items)
        return (
            <div key={items.length + 1} className="total item">
                <span className="total-quantity">
                    {this.props.getTotalItems(items)}
                    {this.props.getTotalItems(items) === 1 ? ' item' : ' itens'}
                </span>
                <span className="total-value">
                    {this.props.parsePrice(totalValue)}
                </span>
            </div>
        )
    }

    renderCheckoutButton() {
        return (
            <Link to={'/checkout/address'}>
                <button className="button-checkout">
                    <MdCheck className="button-checkout-icon" />
                    Checkout
                </button>
            </Link>
        )
    }

    render() {
        let rows = []
        const items = this.props.shoppingCartItems
        const total = items.length > 0 ? this.renderTotal(items) : null
        const checkout = items.length > 0 ? this.renderCheckoutButton() : null
        if (items.length > 0) {
            rows.push(this.renderHeader())
            for (let i = 0; i < items.length; i++) {
                rows.push(this.renderProduct(items[i], i))
            }
        } else {
            rows.push(
                <div key={0} className="empty">
                    <div> Não existem itens no carrinho. </div>
                </div>
            )
        }

        return (
            <div className="App-shopping-cart has-header">
                <div className="list">
                    {rows}
                    {total}
                </div>
                {checkout}
            </div>
        )
    }
}

export default ShoppingCart
