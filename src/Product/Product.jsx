import React from 'react'
import { Link } from 'react-router-dom'
import apiData from '../api.json'
import './Product.scss'
import { MdAddShoppingCart } from 'react-icons/md'

class Product extends React.Component {
    constructor(props) {
        super(props)

        this.apiData = JSON.parse(JSON.stringify(apiData))
    }

    handleClick() {
        this.props.handleAddCartItem(this.props.product)
    }

    render() {
        const product = this.props.product
        let showDetailsButton = ''
        let showDetailsVendor = ''
        let showDetailsDescription = ''
        if (this.props.showDetails) {
            showDetailsVendor = (
                <div>
                    <p className="product-vendor-id-label">Id do vendedor</p>
                    <p className="product-vendor-id-code">{this.apiData.split_rules[0].recipient_id}</p>
                </div>
            )
            showDetailsDescription = <div className="product-description" dangerouslySetInnerHTML={{ __html: product.description }}/>
        }
        if (this.props.showDetailsButton) {
            showDetailsButton = (
                <Link to={'/product-details/' + product.id}>
                    <button className="button-product-details">Ver detalhes</button>
                </Link>
            )
        }
        return (
            <div className="App-product">
                <img alt="Imagem do Produto" src={product.image_path} />
                <p className="product-title">{product.title}</p>
                {showDetailsDescription}
                <p className="product-unit-price">{this.props.parsePrice(product.unit_price)}</p>
                <div className="buttons-wrapper">
                    <button className="button-add-to-cart" onClick={() => this.handleClick()}>
                        <MdAddShoppingCart className="button-add-to-cart-icon" />
                        Comprar
                    </button>
                    {showDetailsButton}
                </div>
                {showDetailsVendor}
            </div>
        )
    }
}

export default Product
