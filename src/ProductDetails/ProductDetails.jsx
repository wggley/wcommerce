import React from 'react'
import './ProductDetails.scss'
import Product from '../Product/Product.jsx'

class ProductDetails extends React.Component {
    render() {
        return (
            <div className="App-product-details has-header">
                <Product
                    showDetails={true}
                    product={this.props.product}
                    handleAddCartItem={this.props.handleAddCartItem}
                    parsePrice={this.props.parsePrice}
                />
            </div>
        )
    }
}

export default ProductDetails
