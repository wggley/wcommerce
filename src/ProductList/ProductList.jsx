import React from 'react'
import './ProductList.scss'
import Product from '../Product/Product.jsx'

class ProductList extends React.Component {
    render() {
        let rows = []
        let data = this.props.productsData
        for (let i = 0; i < data.length; i++) {
            rows.push(
                <li key={data[i].id}>
                    <Product
                        showDetailsButton={true}
                        product={data[i]}
                        handleAddCartItem={this.props.handleAddCartItem}
                        parsePrice={this.props.parsePrice}
                    />
                </li>
            )
        }
        return (
            <div className="App-product-list has-header">
                <ul>
                    {rows}
                </ul>
            </div>
        )
    }
}

export default ProductList
